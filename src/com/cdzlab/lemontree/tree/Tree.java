package com.cdzlab.lemontree.tree;

/**
 * @author arseniy
 */
public class Tree {
    private Node mRoot;
    private StringBuilder mCurrentPath;

    private enum Direction {
        NONE, LEFT, RIGHT
    }

    public Tree() {
        clearCurrentPath();
    }

    public void add(Node node) {
        if (mRoot == null) {
            mRoot = node;
            return;
        }

        Node current = mRoot;
        Node parent;

        while (true) {
            parent = current;
            if (node.getKey() < current.getKey()) {
                current = parent.getLeftChild();
                if (current == null) {
                    parent.setLeftChild(node);
                    return;
                } else {
                    current = parent.getLeftChild();
                }
            } else {
                current = parent.getRightChild();
                if (current == null) {
                    parent.setRightChild(node);
                    return;
                } else {
                    current = parent.getRightChild();
                }
            }
        }
    }

    public Node find(int key) {
        if (mRoot == null) {
            return null;
        }

        clearCurrentPath();

        Node current = mRoot;
        Node parent;
        while (true) {
            parent = current;
            mCurrentPath.append("> " + current.getKey() + " ");
            if (current.getKey() == key) {
                mCurrentPath.append('\n');
                return current;
            } else if (key < current.getKey()) {
                current = parent.getLeftChild();
            } else if (key > current.getKey()) {
                current = parent.getRightChild();
            } else {
                break;
            }
        }

        return null;
    }

    public Node findJointParent(int keyA, int keyB) {
        if (mRoot == null) {
            return null;
        }

        Direction aDir;
        Direction bDir;

        Node current = mRoot;
        Node parent;
        while (true) {
            parent = current;

            if (keyA < current.getKey()) {
                aDir = Direction.LEFT;
            } else {
                aDir = Direction.RIGHT;
            }

            if (keyB < current.getKey()) {
                bDir = Direction.LEFT;
            } else {
                bDir = Direction.RIGHT;
            }

            if (aDir != bDir) {
                return current;
            } else {
                if (aDir == Direction.LEFT) {
                    current = parent.getLeftChild();
                } else {
                    current = parent.getRightChild();
                }
            }

            if (current == null) {
                break;
            }
        }

        return null;
    }

    private void clearCurrentPath() {
        mCurrentPath = new StringBuilder();
    }

    public String getCurrentPath() {
        return mCurrentPath.toString();
    }
}
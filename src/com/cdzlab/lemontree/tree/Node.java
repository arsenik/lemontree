package com.cdzlab.lemontree.tree;

/**
 * @author arseniy
 */
public class Node {
    private int mKey;
    private Node mLeftChild;
    private Node mRightChild;

    private Object mData;

    public Node(int key, Object data) {
        mKey = key;
        mData = data;
    }

    public int getKey() {
        return mKey;
    }

    public Node getLeftChild() {
        return mLeftChild;
    }

    public Node getRightChild() {
        return mRightChild;
    }

    public void setLeftChild(Node leftChild) {
        mLeftChild = leftChild;
    }

    public void setRightChild(Node rightChild) {
        mRightChild = rightChild;
    }
}

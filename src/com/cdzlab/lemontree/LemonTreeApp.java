package com.cdzlab.lemontree;

import com.cdzlab.lemontree.tree.Node;
import com.cdzlab.lemontree.tree.Tree;

/**
 * @author arseniy
 */
public class LemonTreeApp {
    public static void main(String[] args) {
        Tree tree = new Tree();

        tree.add(new Node(77, "A"));
        tree.add(new Node(80, "C"));
        tree.add(new Node(49, "B"));
        tree.add(new Node(11, "D"));
        tree.add(new Node(52, "E"));
        tree.add(new Node(70, "F"));
        tree.add(new Node(74, "G"));

        testFindJointNode(tree, 70, 74);
    }

    private static void testFindNode(Tree tree, int key) {
        System.out.println("Finding path: ");
        Node node = tree.find(key);
        System.out.println("Key: " + key);
    }

    private static void testFindJointNode(Tree tree, int keyA, int keyB) {
        System.out.println("Finding joint parent for: " + keyA + " and " + keyB);
        Node node = tree.findJointParent(keyA,keyB);
        System.out.println("Joint parent is: " + node.getKey());
    }
}
